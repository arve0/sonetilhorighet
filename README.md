# Sonetilhørighet for Trondheim kommune

## Installer
```sh
npm i @arve0/sonetilhorighet
```


## Bruk
```js
import React from "react";
import { NaermesteBarnehage, Sonetilhorighet, Tommekalender } from "@arve0/sonetilhorighet";

const App = () => {
  return (<div>
      <Typography variant="h5" component="h4">Nærmeste barnehager</Typography>
      <NaermesteBarnehage adresse="Okstadvegen 1" />

      <Typography variant="h5" component="h4">Soner</Typography>
      <Sonetilhorighet adresse="Ada Arnfinsens veg 1C" />

      <Typography variant="h5" component="h4">Tømmekalender</Typography>
      <Tommekalender adresse="Ada Arnfinsens veg 1C" />
  </div>)
}
export default App;
```


## Tilbakemelding på henting av ressurser
Komponentene vil sende en action til `dispatch` dersom `dispatch` gis
som prop til komponenten. Eksempelvis:

```js
<Sonetilhorighet adresse="Ada Arnfinsens veg 1C" dispatch={dispatch} />
// sender actions:
// - FETCH_TK-GEOAPI_REQUEST
// - FETCH_TK-GEOAPI_SUCCESS
// - FETCH_TK-GEOAPI_ERROR, feilen sendes som payload
```

Her er `TK-GEOAPI` fra variabelen `SERVICE_NAME` i hver aktuelle (interne) service.


## Oversettelser
Komponenten har standardtekst for norsk bokmål og engelsk. Oversetting kan gjøres med [react-localize-redux](https://github.com/ryandrewjohnson/react-localize-redux), der
`translate` gis til komponenten:

```js
import React from "react";
import { Sonetilhorighet, getTranslations as getSoneTranslations } from "@arve0/sonetilhorighet";
import { addTranslation, getTranslate } from 'react-localize-redux';
import store from "./store";

// Legg til standardoversettelser, eller bruk dine egne.
store.dispatch(addTranslation(getSoneTranslations()));


const App = ({ translate }) => {
  return (<div>
      <Typography variant="h5" component="h4">Soner</Typography>
      <Sonetilhorighet adresse="Ada Arnfinsens veg 1C" translate={translate} />
  </div>)
}

// gi translate-funksjonen fra react-localize-redux til komponenten
const mapStateToProps = ({ locale }) => ({ translate: getTranslate(locale) })
export default connect(mapStateToProps)(App);
```

Oversettelser finnes i _translations.json_, som ligger ved siden av komponentene.
_translations.json_ kan brukes som utgangspunkt for dine egne oversettelser.

Eksempelvis har `Sonetilhorighet` oversettelser i [src/components/sone/translations.json](src/components/sone/translations.json).


## Utvikle

### Kom i gang
```sh
npm i
npm start
```

### Tester
Se tester i [test/test.js](./test/test.js).

Les testdokumentasjon: [test/README.md](test/README.md)