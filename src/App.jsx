import React from 'react';
import styles from './App.css';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import tkTheme from '@trdk/style/dist/Theme'; // eslint-disable-line
import Autocomplete from './Autocomplete';
import { adresseSok } from './service/kartverket';
import { setAdresseInUrlDebounced, getAdresseFromUrl } from './url-state';

const classes = {
  container: styles.search,
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      suggestions: [],
      selectedAddress: getAdresseFromUrl(),
      searchTimeout: null,
    };
  }

  onInputChange = (inputValue) => {
    this.setState({ inputValue, suggestions: [] });
    this.debouncedSearch();
  };

  debouncedSearch = () => {
    const { searchTimeout } = this.state;
    clearTimeout(searchTimeout);

    const newSearchTimeout = setTimeout(() => {
      const { inputValue, selectedAddress } = this.state;
      const searchText = inputValue;

      if (searchText !== selectedAddress) {
        adresseSok(searchText).then((adresser) => this.setState({ suggestions: adresser }));
      }
    }, 300);
    this.setState({ searchTimeout: newSearchTimeout });
  };

  onSelect = (selectedAddress) => {
    this.setState({ selectedAddress });
    setAdresseInUrlDebounced(selectedAddress);
  };

  render() {
    const { suggestions, selectedAddress } = this.state;
    const { renderHeader, renderChildren } = this.props;

    return (
      <MuiThemeProvider theme={tkTheme}>
          <div className={styles.app}>
            {renderHeader && renderHeader()}

            <Autocomplete
              classes={classes}
              placeholder="Søk etter adresse"
              suggestions={suggestions}
              onInputChange={this.onInputChange}
              onSelect={this.onSelect}
            />

            {selectedAddress && renderChildren(selectedAddress)}
          </div>
      </MuiThemeProvider>
    );
  }
}
export default App;
