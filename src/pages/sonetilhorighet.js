import React from 'react';
import ReactDOM from 'react-dom'; // eslint-disable-line
import App from '../App';

import { Typography } from '@material-ui/core';
import SoneTilhorighet from '../components/sone/SoneTilhorighet';

// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(
  <App
    renderHeader={() => (
      <>
        <Typography variant="h3" component="h1">
          Nærområde
        </Typography>
        <Typography variant="h6" component="h2">
          Finn sonetilhørighet for adresse
        </Typography>
      </>
    )}
    renderChildren={(adresse) => (
      <>
        <Typography variant="h4" component="h3">
          {adresse}
        </Typography>

        <SoneTilhorighet adresse={adresse} />
      </>
    )}
  />,
  document.getElementById('tk-sonetilhorighet'),
);
