import React from 'react';
import ReactDOM from 'react-dom'; // eslint-disable-line
import App from '../App';

import { Typography } from '@material-ui/core';
import NaermesteBarnehage from '../components/barnehage/NaermesteBarnehage';

// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(
  <App
    renderHeader={() => (
      <>
        <Typography variant="h3" component="h1">
          Nærmeste barnehage
        </Typography>
        <Typography variant="h6" component="h2">
          Finn nærmeste barnehage til adresse
        </Typography>
      </>
    )}
    renderChildren={(adresse) => (
      <>
        <Typography variant="h4" component="h3">
          {adresse}
        </Typography>

        <NaermesteBarnehage adresse={adresse} />
      </>
    )}
  />,
  document.getElementById('tk-barnehage'),
);
