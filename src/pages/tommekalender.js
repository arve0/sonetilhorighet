import React from 'react';
import ReactDOM from 'react-dom'; // eslint-disable-line
import App from '../App';

import { Typography } from '@material-ui/core';
import Tommekalender from '../components/tommekalender/Tommekalender';

// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(
  <App
    renderHeader={() => (
      <>
        <Typography variant="h3" component="h1">
          Tømmekalender
        </Typography>
        <Typography variant="h6" component="h2">
          Finn tømmekalender til adresse
        </Typography>
      </>
    )}
    renderChildren={(adresse) => (
      <>
        <Typography variant="h4" component="h3">
          {adresse}
        </Typography>

        <Tommekalender adresse={adresse} />
      </>
    )}
  />,
  document.getElementById('tk-tommekalender'),
);
