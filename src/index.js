import React from 'react';
import ReactDOM from 'react-dom'; // eslint-disable-line
import App from './App';

import { Typography } from '@material-ui/core';
import SoneTilhorighet from './components/sone/SoneTilhorighet';
import NaermesteBarnehage from './components/barnehage/NaermesteBarnehage';
import Tommekalender from './components/tommekalender/Tommekalender';

// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(
  <App
    renderChildren={(adresse) => (
      <>
        <Typography variant="h5" component="h4">
          Sonetilhørighet
        </Typography>
        <SoneTilhorighet adresse={adresse} />

        <Typography variant="h5" component="h4">
          Barnehager i nærheten
        </Typography>
        <NaermesteBarnehage adresse={adresse} />

        <Typography variant="h5" component="h4">
          Tømmekalender
        </Typography>
        <Tommekalender adresse={adresse} />
      </>
    )}
  />,
  document.getElementById('root'),
);
