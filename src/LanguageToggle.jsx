import React from 'react';
import PropTypes from 'prop-types';
import { withLocalize } from 'react-localize-redux'; // eslint-disable-line

const LanguageToggle = ({ languages, activeLanguage, setActiveLanguage }) => (
  <div>
    <p>
      Active:
      {activeLanguage}
    </p>
    <ul className="selector">
      {languages.map((lang) => (
        <li key={lang.code}>
          <button type="button" onClick={() => setActiveLanguage(lang.code)}>
            {lang.name}
          </button>
        </li>
      ))}
    </ul>
  </div>
);

LanguageToggle.propTypes = {
  languages: PropTypes.array.isRequired,
  activeLanguage: PropTypes.object.isRequired,
  setActiveLanguage: PropTypes.func.isRequired,
};

export default withLocalize(LanguageToggle);
