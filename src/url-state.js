const PREFIX = 'tksone-adresse:';
const STARTS_WITH_PREFIX = new RegExp(`^${PREFIX}`);
const PREFIX_VALUE_MATCH = new RegExp(`^${PREFIX}(.*)`);

export function getAdresseFromUrl() {
  const hash = window.location.hash.slice(1);

  if (hash.length === 0 || hash.match(STARTS_WITH_PREFIX) === null) {
    return '';
  }

  const [, adresse] = hash.match(PREFIX_VALUE_MATCH);
  return decodeURI(adresse);
}

let previousTimeout;
export function setAdresseInUrlDebounced(text) {
  clearTimeout(previousTimeout);

  previousTimeout = setTimeout(() => {
    if (typeof text === 'string' && text.trim().length) {
      window.location.hash = PREFIX + encodeURI(text);
    } else {
      window.location.hash = '';
    }
  }, 500);
}
