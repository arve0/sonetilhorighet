import React from 'react';
import PropTypes from 'prop-types';
import { Table, TableBody, Button, Paper, TableRow, TableCell } from '@material-ui/core';
import { getCoordinate } from '../../service/kartverket';
import barnehagerRaw from './barnehager.json';
import styles from '../table.css';
import translations from './translations.json';
import getTranslate from '../../service/translate';

const defaultState = {
  coordinate: null,
  barnehager: [],
  showNBarnehager: 5,
};
export default class NaermesteBarnehage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...defaultState };

  }
  componentDidMount() {
    this._ismounted = true;
    this.updateNearestBarnehage(this.props.adresse);
  }

  componentWillUnmount() {
    // TODO: Consider to separate fetching and viewing logic.
    // Pros current solution: Simple usage of component.
    // Cons current solution: Minor memory leak, need to keep component state until promise finishes.
    this._ismounted = false;
  }

  componentWillReceiveProps(nextProps) {
    const { adresse } = this.props;
    if (nextProps.adresse !== adresse) {
      this.setState({ ...defaultState });
      this.updateNearestBarnehage(nextProps.adresse);
    }
  }

  updateNearestBarnehage = (adresse) =>
    getCoordinate(adresse, this.props.dispatch)
      .then((coordinate) => this._ismounted && this.setState({ coordinate }))
      .then(() => this._ismounted && this.updateBarnehageDistance());

  updateBarnehageDistance = () => {
    const { coordinate } = this.state;

    const barnehager = [
      ...barnehagerRaw.map((barnehage) => {
        if (!barnehage.coordinate) {
          console.warn('Ingen koordinat for', barnehage.name); // eslint-disable-line no-console
        }
        const distance = calcDistance(barnehage.coordinate, coordinate);

        return { ...barnehage, distance };
      }),
    ];

    barnehager.sort((a, b) => a.distance - b.distance);
    this.setState({ barnehager });
  };

  render() {
    const { showNBarnehager, barnehager } = this.state;
    const visibleBarnehager = barnehager.slice(0, showNBarnehager);
    const showMoreButton = showNBarnehager < barnehager.length;
    let translate = getTranslate(translations, this.props);

    return (
      <div>
        <Paper className={styles.table}>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell component="th">{translate('naermesteBarnehage.name')}</TableCell>
                <TableCell component="th">{translate('naermesteBarnehage.distance')}</TableCell>
                <TableCell component="th">{translate('naermesteBarnehage.link')}</TableCell>
              </TableRow>
              {visibleBarnehager.map((barnehage) => (
                <BarnehageRow key={barnehage.id} barnehage={barnehage} translate={translate} />
              ))}
            </TableBody>
          </Table>
        </Paper>
        {showMoreButton && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => this.setState({ showNBarnehager: showNBarnehager + 5 })}
          >
            {translate('naermesteBarnehage.showMore')}
          </Button>
        )}
      </div>
    );
  }
}
NaermesteBarnehage.propTypes = {
  adresse: PropTypes.string.isRequired,
  dispatch: PropTypes.func,
};

function BarnehageRow({ barnehage, translate }) {
  return (
    <TableRow>
      <TableCell>{barnehage.name}</TableCell>
      <TableCell>
        {prettyDistance(barnehage.distance, translate('naermesteBarnehage.unknownDistance'))}
      </TableCell>
      <TableCell>
        <Button
          variant="contained"
          color="primary"
          href={`https://www.trondheim.kommune.no${barnehage.url}`}
        >
          {barnehage.name}
        </Button>
      </TableCell>
    </TableRow>
  );
}

function calcDistance(a, b) {
  if (!a || !b) {
    return Infinity;
  }

  // √( (x_a - x_b)² + (y_a - y_b)² )
  return Math.floor(Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2));
}

function prettyDistance(d, fallback) {
  if (!Number.isFinite(d)) {
    return fallback;
  }

  return d < 1000 ? `${d} meter` : `${Math.floor(d / 100) / 10} km`;
}
