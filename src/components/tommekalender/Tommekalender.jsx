import React from 'react';
import PropTypes from 'prop-types';
import fetchPlans from '../../service/tommekalender';
import Plan from './Plan';

const defaultState = {
  plans: [],
};

export default class Tommekalender extends React.Component {
  constructor(props) {
    super(props);
    this.state = defaultState;
  }

  componentDidMount() {
    this._ismounted = true;
    this.updatePlan(this.props.adresse);
  }

  componentWillUnmount() {
    // TODO: Consider to separate fetching and viewing logic.
    // Pros current solution: Simple usage of component.
    // Cons current solution: Minor memory leak, need to keep component state until promise finishes.
    this._ismounted = false;
  }

  componentWillReceiveProps(nextProps) {
    const { adresse } = this.props;
    if (nextProps.adresse !== adresse) {
      this.setState({ ...defaultState });
      this.updatePlan(nextProps.adresse);
    }
  }

  updatePlan = (adresse) =>
    fetchPlans(adresse, this.props.dispatch).then((plans) => this._ismounted && this.setState({ plans }));

  render() {
    const { plans } = this.state;
    const { translate } = this.props;

    return (
      <div>
        {plans.map((plan, i) => (
          <Plan key={plan.id} index={i} plan={plan} translate={translate} />
        ))}
      </div>
    );
  }
}

Tommekalender.propTypes = {
  adresse: PropTypes.string.isRequired,
  translate: PropTypes.func,
  dispatch: PropTypes.func,
};
