import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  Table,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import translations from './translations.json';
import getTranslate from '../../service/translate';
import styles from './Plan.css';
import tableStyle from '../table.css';

const defaultState = {
  numberOfWeeks: 4,
  filter: null,
};

export default class Plan extends React.Component {
  constructor(props) {
    super(props);
    this.state = defaultState;
  }

  render() {
    const { plan, index } = this.props;
    const { numberOfWeeks, filter } = this.state;
    const translate = getTranslate(translations, this.props);
    const shown = plan.calendar
      .filter((week) => (filter === null ? true : week.wastetype.includes(filter)))
      .slice(0, numberOfWeeks);
    const hasMore = plan.calendar.length > numberOfWeeks;
    const descriptionNames = plan.descriptions.map((description) => description.name);

    return (
      <ExpansionPanel defaultExpanded={index === 0} className={styles.plan}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{translate(`tommekalender.${plan.type}`, { name: plan.name })}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={styles.details}>
          <Button
            variant="contained"
            color={filter === null ? 'primary' : 'secondary'}
            onClick={() => this.setState({ filter: null })}
          >
            Vis alle
          </Button>
          {descriptionNames.map((description) => (
            <Button
              variant="contained"
              color={filter === description ? 'primary' : 'secondary'}
              key={description}
              onClick={() => this.setState({ filter: description })}
            >
              {description}
            </Button>
          ))}
          <Table className={tableStyle.table}>
            <TableBody>
              <TableRow>
                <TableCell component="th">{translate('tommekalender.week')}</TableCell>
                <TableCell component="th">{translate('tommekalender.wasteType')}</TableCell>
                <TableCell component="th">{translate('tommekalender.date')}</TableCell>
              </TableRow>
              {shown.map((entry) => WeekRow(plan, entry))}
            </TableBody>
          </Table>
          {hasMore && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.setState({ numberOfWeeks: numberOfWeeks + 5 })}
            >
              {translate('tommekalender.showMore')}
            </Button>
          )}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

Plan.propTypes = {
  plan: PropTypes.shape({
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    calendar: PropTypes.array.isRequired,
  }),
};

function WeekRow(plan, entry) {
  return (
    <TableRow key={`${plan.id}-${entry.week}`}>
      <TableCell>{entry.week}</TableCell>
      <TableCell>{entry.wastetype}</TableCell>
      <TableCell>
        {entry.date_week_start} -{entry.date_week_end}
      </TableCell>
    </TableRow>
  );
}
