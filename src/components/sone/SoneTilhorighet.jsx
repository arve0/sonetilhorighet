import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography,
  Table,
  TableBody,
  Button,
  Paper,
  TableRow,
  TableCell,
} from '@material-ui/core';
import finnSoner from '../../service/tk-geoapi';
import styles from '../table.css';
import translations from './translations.json';
import getTranslate from '../../service/translate';

const defaultState = {
  soner: [],
};
export default class SoneTilhorighet extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...defaultState };
  }

  componentDidMount() {
    this._ismounted = true;
    this.updateSoner(this.props.adresse);
  }

  componentWillUnmount() {
    // TODO: Consider to separate fetching and viewing logic.
    // Pros current solution: Simple usage of component.
    // Cons current solution: Minor memory leak, need to keep component state until promise finishes.
    this._ismounted = false;
  }

  componentWillReceiveProps(nextProps) {
    const { adresse } = this.props;
    if (nextProps.adresse !== adresse) {
      this.setState({ ...defaultState });
      this.updateSoner(nextProps.adresse);
    }
  }

  updateSoner = (adresse) => {
    const { dispatch } = this.props;

    finnSoner(adresse, dispatch).then((soner) => this._ismounted && this.setState({ soner }));
  };

  render() {
    const { soner } = this.state;
    let translate = getTranslate(translations, this.props);

    return (soner.length === 0
      ? <div></div>
      : <div>
          <Typography variant="body1">{translate('sonetilhorighet.body')}</Typography>
          <Paper className={styles.table}>
            <Table>
              <TableBody>
                {soner.map((sone) => (
                  <SoneInformation key={sone.navn} sone={sone} translate={translate} />
                ))}
              </TableBody>
            </Table>
          </Paper>
        </div>
    );
  }
}

SoneTilhorighet.propTypes = {
  adresse: PropTypes.string.isRequired,
  translate: PropTypes.func,
  dispatch: PropTypes.func,
};

function SoneInformation({ sone, translate }) {
  return (
    sone && (
      <TableRow>
        <TableCell component="th" scope="row">
          {translate(`sonetilhorighet.${sone.navn}`)}
        </TableCell>
        <TableCell>{sone.verdi}</TableCell>
        <TableCell>
          {sone.lenke && (
            <Button variant="contained" color="primary" href={sone.lenke}>
              {translate('sonetilhorighet.information-about', { value: sone.verdi })}
            </Button>
          )}
        </TableCell>
      </TableRow>
    )
  );
}

SoneInformation.propTypes = {
  translate: PropTypes.func.isRequired,
  sone: PropTypes.shape({
    navn: PropTypes.string,
    verdi: PropTypes.string,
    lenke: PropTypes.string,
  }).isRequired,
};
