import leven from 'leven';
import { fetchJSON, toRequestQuery } from './fetchJSON';
import { reportFetchSuccess, reportFetchError, reportFetchRequest } from './fetch.actions';

const BASE_URL = 'https://trv.no/wp-json/wasteplan/v1/';
const SERVICE_NAME = 'tommekalender';

const noop = () => {};
export default async function fetchPlans(address, dispatch = noop) {
  dispatch(reportFetchRequest(SERVICE_NAME));

  try {
    const locations = await fetchLocations(address);
    let plans = await Promise.all(locations.map((location) => fetchCalendar(location, address)));
    plans = plans.filter((p) => p !== null);

    plans.sort(sortByNameDistanceToNumber(address));
    bestMatchFirst(plans, address);

    dispatch(reportFetchSuccess(SERVICE_NAME));
    return plans;
  } catch (error) {
    dispatch(reportFetchError(SERVICE_NAME, error));
  }
}

async function fetchLocations(address) {
  if (!address || address.length === 0) {
    return [];
  }

  /**
   * API interprets spaces as or, such that a search
   * for "general buddes gate" gives any streets matching
   * either general, buddes or gate.
   *
   * The API has a special case for veg and vei, where the
   * word is removed from the search, but this special case
   * does not exists for gate.
   *
   * Strategy:
   * 1. Remove " gate"
   * 2. Filter results, such that they contain
   *    all words in search address.
   */

  // step 1 in strategy from above
  const s = address.replace(/ gate([^a-zæøå0-9])?/, '$1');

  const query = toRequestQuery({ s });
  let locations = await fetchJSON(`${BASE_URL}locations${query}`);

  // step 2 in strategy from above
  locations = removeLocationsNotMatchingAddress(locations, address);

  if (!locations || !Array.isArray(locations) || locations.length === 0) {
    throw new Error(`Did not find any locations for ${address}`);
  }

  return locations.map((l) => ({
    id: l.id,
    name: l.name,
  }));
}

async function fetchCalendar(location) {
  const plan = await fetchJSON(`${BASE_URL}calendar/${location.id}`);

  if (!plan || !Array.isArray(plan.calendar) || plan.calendar.length === 0) {
    // eslint-disable-next-line no-console
    console.error(`Did not find calendar for ${location.name} (${location.id})`);
    return null;
  }

  const calendar = removeOrMergeDuplicates(plan.calendar);
  const descriptions = getAllDescriptions(plan.calendar);
  removeCalendarDescriptions(plan.calendar);

  return {
    id: plan.id,
    name: plan.name,
    type: normalizeType(plan.type),
    calendar,
    descriptions,
  };
}

function normalizeType(type) {
  const allowed = ['bins', 'containers'];
  const normalized = type.toLowerCase();

  if (allowed.includes(normalized)) {
    return normalized;
  }
  // eslint-disable-next-line no-console
  console.warn(`Unknown type ${type}, expected one of ${allowed}`);
  return 'unknown';
}

/**
 * API can give same week more then once. Upon duplicate weeks,
 * remove "No pickup" and merge others.
 */
function removeOrMergeDuplicates(calendar) {
  return calendar.reduce((filtered, entry) => {
    if (filtered.some((e) => sameYearAndWeek(e, entry))) {
      return filtered;
    }

    const allOfSameWeek = calendar.filter((e) => sameYearAndWeek(e, entry));
    if (allOfSameWeek.length === 1) {
      filtered.push(entry);
      return filtered;
    }

    // "No pickup" has no description
    const withoutNoPickup = allOfSameWeek.filter((e) => e.description !== null);
    if (withoutNoPickup.length === 1) {
      filtered.push(withoutNoPickup[0]);
      return filtered;
    }

    const mergedEntry = {
      year: entry.year,
      week: entry.week,
      date: entry.date,
      description: withoutNoPickup.reduce(
        (description, value) => ({ ...description, ...value.description }),
        {},
      ),
      wastetype: withoutNoPickup.map((e) => e.wastetype).join(', '),
      wastetype_en: withoutNoPickup.map((e) => e.wastetype_en).join(', '),
      date_week_start: entry.date_week_start,
      date_week_end: entry.date_week_end,
      day_of_week: entry.day_of_week,
    };
    filtered.push(mergedEntry);

    return filtered;
  }, []);
}

function sameYearAndWeek(a, b) {
  return a.week === b.week && a.year === b.year;
}

/**
 * If we get a perfect match on plan name and address,
 * make sure that plan is the first one in list.
 *
 * Example:
 *
 * Searching for "okstadøy" gives the posibilities:
 * - "Okstadøy (bins)"
 * - "Okstadøy 2 , 4 (container)"
 * - "Okstadøy 45 (container)"
 * - "Okstadøy 47 (container)"
 * - "Okstadøy 76 (container)"
 *
 * Expected results are:
 * - "Okstadøy 45" -> "Okstadøy 45 (container)"
 * - "Okstadøy 1" -> "Okstadøy (bins)"
 * - "Okstadøy 2" -> "Okstadøy 2 , 4 (container)"
 * - "Okstadøy 4" -> "Okstadøy 2 , 4 (container)"
 */
function bestMatchFirst(plans, address) {
  const exactMatch = plans.findIndex((p) => p.name === address);

  if (exactMatch !== -1) {
    moveToFront(exactMatch, plans);
    return;
  }

  const partialMatch = plans.findIndex((plan) => nameAndNumberMatches(plan.name, address));
  if (partialMatch !== -1) {
    moveToFront(partialMatch, plans);
    return;
  }

  const binMatch = plans.findIndex((plan) => isBinAndNameMatches(plan, address));
  if (binMatch !== -1) {
    moveToFront(binMatch, plans);
    return;
  }

  // eslint-disable-next-line no-console
  console.warn('No partial match for name, assuming first bin is correct');
  const firstBin = plans.findIndex(isBin);
  if (firstBin !== -1) {
    moveToFront(firstBin, plans);
    return;
  }

  // eslint-disable-next-line no-console
  console.warn('No bins found, only sorted by name number');
}

function nameAndNumberMatches(planName, address) {
  const planNumbers = numbersInText(planName);
  const addressNumbers = numbersInText(address);

  return (
    withoutNumberOrComma(planName) === withoutNumberOrComma(address) &&
    planNumbers.some((n) => addressNumbers.includes(n))
  );
}

function withoutNumberOrComma(address) {
  return address.match(/^[^0-9,]+/)[0].trim();
}

function numbersInText(address) {
  return (address.match(/[0-9]+/g) || []).map(parseInt);
}

function moveToFront(index, array) {
  const toMove = array.splice(index, 1);
  array.unshift(...toMove);
}

function isBinAndNameMatches(plan, address) {
  const planName = plan.name.toLowerCase();
  const addressName = withoutNumberOrComma(address).toLowerCase();
  return isBin(plan) && planName.startsWith(addressName);
}

function isBin(plan) {
  return plan.type === 'bins';
}

function sortByNameDistanceToNumber(address) {
  return (a, b) => {
    const aName = withoutNumberOrComma(a.name);
    const bName = withoutNumberOrComma(b.name);

    if (aName < bName) {
      return -1;
    }
    if (aName > bName) {
      return 1;
    }
    const addressNumber = firstNumberInText(address);
    const aNumberDistance = Math.abs(firstNumberInText(a.name) - addressNumber);
    const bNumberDistance = Math.abs(firstNumberInText(b.name) - addressNumber);
    return aNumberDistance - bNumberDistance;
  };
}

function firstNumberInText(text) {
  return numbersInText(text)[0] || -1000;
}

/**
 * Computes edit distance between words in address and location name.
 * All words in address has a edit distance equal or below 2, we
 * consider it a match.
 */
function removeLocationsNotMatchingAddress(locations, address) {
  const wordsInAddress = wordsThatAreNotNumbers(address);

  return locations.filter(
    (location) => worstEditDistanceToAnyWordIn(location.name, wordsInAddress) <= 2,
  );
}

function wordsThatAreNotNumbers(text) {
  return text.split(' ').filter(isNotANumber);
}

function isNotANumber(word) {
  return word.match(/[0-9]/) === null;
}

function worstEditDistanceToAnyWordIn(text, wordsToMatch) {
  const wordsInText = wordsThatAreNotNumbers(text);
  const minimumEditDistances = wordsToMatch.map((wordToMatch) =>
    Math.min(...wordsInText.map((w) => leven(w, wordToMatch))),
  );

  return Math.max(...minimumEditDistances);
}

function getAllDescriptions(calendar) {
  let allTypes = calendar
    .map((week) => week.wastetype)
    // some weeks have joined wastetype -> join + split
    .join(', ')
    .split(', ');
  let allDescriptions = calendar.reduce(
    (descriptions, week) => ({
      ...descriptions,
      ...week.description,
    }),
    {},
  );

  let descriptions = unique(allTypes).map((name) => ({
    name,
    description: allDescriptions[name],
  }));

  descriptions.sort(byName);

  return descriptions;
}

function removeCalendarDescriptions(calendar) {
  calendar.forEach((week) => delete week.description);
}

function unique(array) {
  return Array.from(new Set(array));
}

function byName(a, b) {
  return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
}
