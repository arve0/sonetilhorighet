/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift'; // eslint-disable-line
import TextField from '@material-ui/core/TextField';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';

let popperNode = null;

const Autocomplete = (props) => {
  const { classes, placeholder, suggestions, onInputChange, onSelect } = props;

  return (
    <Downshift onInputValueChange={onInputChange} onSelect={onSelect}>
      {({
        getInputProps,
        getItemProps,
        getMenuProps,
        highlightedIndex,
        inputValue,
        isOpen,
        selectedItem,
      }) => (
        <div className={classes.container}>
          {renderInput({
            fullWidth: true,
            InputProps: getInputProps({
              placeholder,
              className: classes.input,
            }),
            ref: (node) => {
              popperNode = node;
            },
          })}
          <Popper open={isOpen} anchorEl={popperNode}>
            <div {...(isOpen ? getMenuProps({}, { suppressRefError: true }) : {})}>
              <Paper
                square
                style={{
                  marginTop: 8,
                  width: popperNode ? popperNode.clientWidth : null,
                }}
              >
                {getSuggestions(suggestions, inputValue).map((suggestion, index) =>
                  renderSuggestion({
                    suggestion,
                    index,
                    itemProps: getItemProps({ item: suggestion }),
                    highlightedIndex,
                    selectedItem,
                    classes,
                  }),
                )}
              </Paper>
            </div>
          </Popper>
        </div>
      )}
    </Downshift>
  );
};

Autocomplete.propTypes = {
  classes: PropTypes.object,
  inputId: PropTypes.string,
  suggestions: PropTypes.arrayOf(PropTypes.string).isRequired,
  onInputChange: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
};

function renderInput(inputProps) {
  const { InputProps, ref, ...other } = inputProps;

  return (
    <TextField
      InputProps={{
        inputRef: ref,
        ...InputProps,
      }}
      {...other}
    />
  );
}

function renderSuggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem,
  classes,
}) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || '').indexOf(suggestion) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
      className={classes.suggestion}
    >
      {suggestion}
    </MenuItem>
  );
}
renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.string.isRequired,
};

function getSuggestions(suggestions, value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter((suggestion) => {
        const keep = count < 5 && suggestion.slice(0, inputLength).toLowerCase() === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}

export default Autocomplete;
