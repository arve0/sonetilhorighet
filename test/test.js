const assert = require('assert');
const configuration = require('../package.json');
const puppeteer = configuration.dependencies.puppeteer
  ? require('puppeteer')
  : require('puppeteer-firefox');

// configuration
const mainPage = `http://localhost:3001/`;
const headless = false; // false: show browser, true: hide browser
const slowMo = false // true: each browser action will take 100 milliseconds
  ? 100
  : 0;
const INPUT_SELECTOR = '.sonetilhorighet-adresse-sok input';

// globals
let browser = null;
let page = null;

before(async function() {
  this.timeout(10 * 1000); // starting browser may take more than default timeout (2 seconds)

  browser = await puppeteer.launch({ headless, slowMo });
  page = (await browser.pages())[0];
  await page.goto(mainPage);

  page.on('console', async function(msg) {
    if (msg.type() === 'error' && msg.args().length) {
      let args = await Promise.all(msg.args().map(arg => arg.jsonValue()));
      console.error('Browser console.error:', ...args);
    } else {
      if (msg._text !== undefined) {
        console.log(msg._text);
      }
    }
  });
});

after(function() {
  if (slowMo !== 0) {
    setTimeout(() => browser.close(), 2000);
  } else {
    browser.close();
  }
});

describe('tests', function() {
  beforeEach(async function() {
    await reset_and_activate_input();
  });

  this.timeout(slowMo === 0 ? 3000 : 0);

  describe('Adressesøk', function() {
    it('har søkefelt som tar i mot input', async function() {
      await page.keyboard.type('as');
      let input_value = await page.$eval(INPUT_SELECTOR, input => input.value);
      assert.equal(input_value, 'as');
    });

    it('foreslår adresser', async function() {
      console.time('foreslår');
      await page.keyboard.type('ok');
      await waitFor({ text: 'Okstadøy' });
    });

    it('kan velge adresse', async function() {
      await page.keyboard.type('ok');
      await chooseOption('Okstadøy');
      await waitFor({ selector: 'h3', text: 'Okstadøy' });
    });

    it('søk med adresse og nummer', async function() {
      await page.keyboard.type('Elgeseter gate 45');
      await chooseOption('Elgeseter gate 45');
    });

    it('søker uten nummer når ingen treff', async function() {
      // Asbjørnsens gate 1 eksisterer ikke
      await page.keyboard.type('Asbjørnsens gate');
      await chooseOption('Asbjørnsens gate');
    });
  });

  describe('Sonetilhørighet', function() {
    it('skal gi bydel til adresse', async function() {
      await page.keyboard.type('Thomas Angells gate 1');
      await chooseOption('Thomas Angells gate 1');
      await waitFor({ text: 'Midtbyen' });
    });

    it('skal gi barneskole til adresse', async function() {
      // TODO: Kalvskinnet har ingen barneskoler
      // -> Ila?
      await page.keyboard.type('Thomas Angells gate 2');
      await chooseOption('Thomas Angells gate 2');
      await waitFor({ text: 'Kalvskinnet skole' });
    });

    it('skal ha lenke til barneskole', async function() {
      this.timeout(10 * 1000);
      await page.keyboard.type('Carl W. Ianssens veg 1');
      await chooseOption('Carl W. Ianssens veg 1');
      await waitFor({
        selector: 'a',
        text: 'Informasjon om Hallset skole',
        click: true
      });
      await waitFor({ text: '72545300', timeout: 8 * 1000 });
      await page.goto(mainPage);
    });

    it('skal endre æøå i lenke til barneskole', async function() {
      // TODO: Sørborgen skole i Klæbu
      // for øyeblikket feiler alle adresser i Klæbu mot tk-geoapi
      // finnes ingen skoler med æ
      this.timeout(10 * 1000);
      await page.keyboard.type('Fagertunvegen 2');
      await chooseOption('Fagertunvegen 2');
      await waitFor({
        selector: 'a',
        text: 'Informasjon om Åsveien skole',
        click: true
      });
      await waitFor({ text: '72544240', timeout: 8 * 1000 });
      await page.goto(mainPage);
    });

    it('skal gi ungdomsskole til adresse', async function() {
      await page.keyboard.type('Dvergstien 1');
      await chooseOption('Dvergstien 1');
      await waitFor({ text: 'Selsbakk ungdomsskole' });
    });

    it('skal ha lenke til ungdomsskole', async function() {
      this.timeout(10 * 1000);
      await page.keyboard.type('Gisle Johnsons gate 1');
      await chooseOption('Gisle Johnsons gate 1');
      await waitFor({
        selector: 'a',
        text: 'Informasjon om Rosenborg ungdomsskole',
        click: true
      });
      await waitFor({ text: '72542106', timeout: 8 * 1000 });
      await page.goto(mainPage);
    });

    it('skal finne skole som er feilstavet', async function() {
      await page.keyboard.type('Magnus blindes');
      await chooseOption('Magnus Blindes');

      // Feilstavet som Blussuvold i tk-geoapi
      await waitFor({ selector: 'a', text: 'Informasjon om Blussuvol' });
    });

    it('skal prøve kun gatenavn når søk på eksakt adresse feiler', async function() {
      await page.keyboard.type('General Buddes gate 12C');
      await chooseOption('General Buddes gate 12C');
      await waitFor({ text: 'Bispehaugen skole' });
    });

    it('skal prøve små bokstaver når gatenavn ikke er eksakt lik', async function() {
      await page.keyboard.type('General von Hovens');
      await chooseOption('General von Hovens');
      await waitFor({ text: 'Østbyen' });
    });

    it('skal finne helsestasjon', async function() {
      await page.keyboard.type('Okstadvegen');
      await chooseOption('Okstadvegen');
      await waitFor({ selector: "a", text: 'Informasjon om Romolslia helsestasjon' });
    });
  });

  describe('Barnehage', function() {
    it('skal ha lenke til nærmeste barnehage', async function() {
      this.timeout(10 * 1000);
      await page.keyboard.type('Ilsvikveien 1');
      await chooseOption('Ilsvikveien 1');
      await waitFor({
        selector: 'a',
        text: 'Ilabekken barnehager',
        click: true
      });
      await waitFor({ text: '41726100', timeout: 8 * 1000 });
      await page.goto(mainPage);
    });
  });

  describe('Tømmekalender', function() {
    this.timeout(10 * 1000);

    it('skal vise tømmekalender', async function() {
      await page.keyboard.type('molt');
      await chooseOption('Moltmyra');
      await waitFor({ text: 'Restavfall' });
    });

    it('første tømmekalender ved eksakt likt navn', async function() {
      await page.keyboard.type('Moltmyra 122');
      await chooseOption('Moltmyra 122');
      await waitFor({ text: 'Restavfall' });
      let options = await page.$$eval('.tommekalender-title', titles =>
        titles.map(title => title.innerText)
      );
      assert(options[0].includes('Moltmyra 122'));
    });

    it('skal falle tilbake på beholder når ekstakt treff ikke finnes', async function() {
      await page.keyboard.type('Moltmyra 44');
      await chooseOption('Moltmyra 44');
      await waitFor({ text: 'Restavfall' });
      let options = await page.$$eval('.tommekalender-title', titles =>
        titles.map(title => title.innerText)
      );

      assert.equal(options[0], 'Beholdere i Moltmyra');
    });

    it('skal sortere etter "nærhet"', async function() {
      // nærhet er her navn -> nummer
      await page.keyboard.type('Moltmyra 39');
      await chooseOption('Moltmyra 39');
      await waitFor({ text: 'Restavfall' });
      let options = await page.$$eval('.tommekalender-title', titles =>
        titles.map(title => title.innerText)
      );

      assert(options[1].startsWith('Container ved Moltmyra 40'));
    });

    it('skal ikke gi hundrevis av treff på "gate"', async function() {
      this.timeout(100 * 1000);
      await page.keyboard.type('general buddes');
      await chooseOption('General Buddes');
      await waitFor({ text: 'Restavfall', timeout: 90 * 1000 });
      let options = await page.$$eval('.tommekalender-title', titles =>
        titles.map(title => title.innerText)
      );

      assert(options.length < 10);
    });

    it('skal ikke krasje når api-et ikke har tømmekalender', async function() {
      await page.keyboard.type('Asbjørnsens gate 23');
      await chooseOption('Asbjørnsens gate 23');
      await waitFor({ text: 'Restavfall' });
    });

    it('skal fjerne treff som er resultat av eller-søk', async function() {
      await page.keyboard.type('general buddes');
      await chooseOption('General Buddes');
      await waitFor({ text: 'Restavfall' });

      let options = await page.$$eval('.tommekalender-title', titles =>
        titles.map(title => title.innerText)
      );
      let otherGenerals = ['Bangs', 'Hovens', 'Wibes'];
      assert(
        !options.some(option =>
          otherGenerals.some(general => option.includes(general))
        )
      );
    });

    it('skal ikke filtrere bort tømmesteder som er feilstavet', async function() {
      await page.keyboard.type('Aasta');
      await chooseOption('Aasta Hansteens veg');
      await waitFor({ text: 'Restavfall' });

      let options = await page.$$eval('.tommekalender-title', titles =>
        titles.map(title => title.innerText)
      );

      // Beholdere i Aasta Hansteens veg er feilstavet som både "Åsta" og "Hanstens",
      // og blir derfor ikke med uten sammenligning av redigeringsdistanse
      assert(options.some(option => option.includes('Beholdere i')));
    });

    it('har filtrering på type avfall', async function() {
      await page.keyboard.type('Olav Duuns veg');
      await chooseOption('Olav Duuns veg');
      await waitFor({ text: 'Restavfall' });

      let filters = await page.$$eval('.tommeplan-details button', buttons =>
        buttons.map(button => button.innerText.toLowerCase())
      );

      assert(filters.includes("restavfall"))
      assert(filters.includes("papir"))
      assert(filters.includes("plastemballasje"))
      assert(filters.includes("hageavfall"))

      await waitFor({ selector: "button", text: "Hageavfall", click: true })
      await waitFor({ selector: "td", text: "Hageavfall" })
    });

    it('viser ikke filtrering for type avfall som ikke hentes', async function() {
      await page.keyboard.type('General Buddes gate');
      await chooseOption('General Buddes gate');
      await waitFor({ text: 'Restavfall' });

      let filters = await page.$$eval('.tommeplan-details button', buttons =>
        buttons.map(button => button.innerText.toLowerCase())
      );

      assert(!filters.includes("papir"))
      assert(!filters.includes("plastemballasje"))
    });
  });
});

async function reset_and_activate_input() {
  await waitFor({ selector: INPUT_SELECTOR });
  // page ready
  await page.$eval(INPUT_SELECTOR, input => (input.value = ''));
  await waitFor({ selector: '[placeholder^="Søk etter"]', click: true });
}

function chooseOption(text) {
  return waitFor({ text, selector: '[role="option"]', click: true });
}

/**
 * Waits for a visible element containing given text, possibly clicks it.
 *
 * @param {object} params - What to wait for, in which selector, if it should be clicked and how long to wait.
 * @param {string} params.text - What text to wait for.
 * @param {string} params.selector - What selector to use, defaults to any element: `*`.
 * @param {bool} params.click - Wheter to click when found.
 * @param {number} params.timeout - How long to wait in milliseconds.
 */
async function waitFor({
  text = '',
  selector = '*',
  click = false,
  focus = false,
  timeout = 2000
}) {
  const start = Date.now();

  while (Date.now() - start < timeout) {
    let frames = await page.frames();
    let scopes = [page, ...frames];
    for (let scope of scopes) {
      let result;
      try {
        result = await scope.evaluate(pageFunction, text, selector, click);
      } catch (err) {
        // probably lost execution context, break and get new scopes
        break;
      }
      if (result) {
        return true;
      }
    }
  }

  throw new Error(
    `'${text}' not found on page in selector '${selector}', waited ${timeout} milliseconds.`
  );

  function pageFunction(text, selector, click) {
    let match = findElement(text, selector);

    if (match) {
      if (click) {
        match.click();
      }
      if (focus) {
        match.focus();
      }
      return true;
    }
    return false;

    function findElement(text, selector) {
      let matchingElements = Array.from(document.querySelectorAll(selector))
        .filter(element => element.textContent.includes(text))
        .sort(shortestTextVisibleIfSameLength); // shortest text first, e.g. "best" search result

      if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
        return matchingElements[0];
      }

      let shadowedElements = Array.from(document.querySelectorAll(selector))
        .filter(element => element.shadowRoot)
        .flatMap(element =>
          Array.from(element.shadowRoot.querySelectorAll(selector))
        )
        .filter(element => element.textContent.includes(text))
        .sort(shortestTextVisibleIfSameLength);

      if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
        return shadowedElements[0];
      }

      return null;
    }

    function shortestTextVisibleIfSameLength(a, b) {
      let difference = a.textContent.length - b.textContent.length;
      if (difference === 0) {
        let aVisible = isVisible(a);
        let bVisible = isVisible(b);

        if (aVisible && !bVisible) {
          return -1;
        } else if (!aVisible && bVisible) {
          return 1;
        } else {
          return 0;
        }
      }
      return difference;
    }

    function isVisible(element) {
      return element.offsetParent !== null;
    }
  }
}
