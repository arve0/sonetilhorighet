# Tester

Dette er et testoppsett for ende-til-ende-testing med firefox/chromium som bruker

- [puppeteer](https://www.npmjs.com/package/puppeteer/) og
- [mocha](https://www.npmjs.com/package/mocha).

Testoppsettet vil

1. Starte firefox eller chromium gjennom puppeteer
2. Kjøre testene med mocha

## Hvordan kjøre tester?

Forsikre at dev-server kjører:

```sh
npm start
```

Kjør testene i en annen terminal:

```sh
$ npm test

> sonetilhorighet@0.1.0 test /Users/arve/KNOWIT-git/sonetilhorighet
> mocha test/test.js



  tests
    ✓ har søkefelt som tar i mot input (62ms)
    ✓ foreslår adresser (462ms)
    ✓ kan velge adresse (93ms)
    ✓ søk med nummer (571ms)
    ✓ skal gi bydel til adresse (1051ms)


  5 passing (5s)
```

## Hvordan kjøre en spesifikk test?

```sh
$ npm test -- -f bydel

> sonetilhorighet@0.1.0 test /Users/arve/KNOWIT-git/sonetilhorighet
> mocha test/test.js "-f" "bydel"



  tests
    ✓ skal gi bydel til adresse (1186ms)
```

## Hvordan ser testene ut?

```js
it("skal gi bydel til adresse", async function() {
  await page.keyboard.type("Thomas Angells gate 1");
  await waitFor({ text: "Thomas Angells gate 1", click: true });
  await waitFor({ text: "Midtbyen" });
});
```

## Konfigurasjon

Les toppen av [test.js](test.js):

```js
// configuration
const mainPage = `http://localhost:3000/`;
const headless = false; // false: show browser, true: hide browser
const slowMo = true // true: each browser action will take 100 milliseconds
  ? 100
  : 0;
```

### Bruk chromium istedenfor firefox

1. Endre `package.json` til å bruke `puppeteer`:

```json
  "dependencies": {
    ...
    "puppeteer": "*",
    ...
  },
```

2. Installer:

```sh
npm install && npm prune
```

3. Kjør tester:

```sh
npm test
```
